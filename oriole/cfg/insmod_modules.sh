#/bin/sh
insmod /vendor/soc_platform/modules/chipone-tddi-icnl9916.ko
insmod /vendor/soc_platform/modules/sprd_wlan_combo.ko
insmod /vendor/soc_platform/modules/sprdbt_tty.ko
insmod /vendor/soc_platform/modules/vpu.ko
insmod /vendor/soc_platform/modules/sprd_vdsp.ko
insmod /vendor/soc_platform/modules/npu_img_vha.ko
sleep 3
chmod 775 /sys/class/rfkill/rfkill1/state
chown blue_host:blue_host /sys/class/rfkill/rfkill1/state
chown blue_host:blue_host /dev/ttyBT0
chown blue_host:blue_host /proc/bluetooth/sleep/lpm
chown blue_host:blue_host /proc/bluetooth/sleep/btwrite
chown blue_host:blue_host /sys/kernel/debug/gpio
chown blue_host:blue_host /sys/devices/70000000.uart/uart_conf
chown bluetooth:bluetooth /dev/uhid
chown codec_host:codec_host /dev/sprd_vsp

chmod 660 /sys/class/devfreq/scene-frequency/sprd-governor/scenario_dfs
chown system:system /sys/class/devfreq/scene-frequency/sprd-governor/scenario_dfs
chmod 660 /sys/class/devfreq/scene-frequency/sprd-governor/exit_scene
chown system:system /sys/class/devfreq/scene-frequency/sprd-governor/exit_scene

chown camera_host camera_host /dev/vpu_enc0
chown camera_host camera_host /dev/vpu_enc1
