# device_board_hardmony

#### 介绍

本仓用于存放基于展锐芯片的开发板相关内容，目前支持基于P7885 SOC的OHarmPi（欧哈派）设备，是北向应用开发的调试利器。

OHarmPi（欧哈派）设备规格如下：
|          |                                            |
| -------- | ------------------------------------------ |
| 操作系统 | OpenHarmony-4.1-Release                     |
| 内存     | 8GB + 256GB                                |
| 屏幕     | 6.72" FHD, 1080*2400, 水滴屏， 90HZ， LTPS  |
   
#### 目录
```
device_board_hardmony
├── oriole                                # OHarmPi（欧哈派）开发者手机
    ├── audio_alsa                        # 设备的媒体内容目录
        ├── audio.param.c                          
    ├── cfg                               # 7885配置文件
    ├── updater                           # 更新配置文件
    ├── build.gn                          # 编译脚本文件，编译主文件
    ├── bundle.json                       # 编译脚本文件
    ├── config.gni                        # 配置内容脚本文件
    ├── device.gni                        # 设备脚本文件
    ├── ohos.build                        # 编译脚本文件            
```


#### 编译构建方法

* xxxx
* xxxx
* xxxx


#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 相关仓

1. [vendor_hardmony](https://gitee.com/openharmony-sig/vendor_hardmony)
2. [device_soc_spreadtrum](https://gitee.com/openharmony-sig/device_soc_spreadtrum)
