# device_board_hardmony

#### Description
{**This warehouse is used to store the related content of the development board based on the Zanrui chip, and currently supports the OHarmPi (Ohapi) equipment based on the P7885 SOC, which is a debugging tool for northbound application development**}

OHarmPi equipment specifications are as follows:
|            |                                            |
| ---------- | ------------------------------------------ |
| Operating system | OpenHarmony-4.1-Release              |
| memory     | 8GB + 256GB                                |
| screen     | 6.72" FHD, 1080*2400, Water drop screen， 90HZ， LTPS |


#### catalogue

```
device_board_hardmony
├── oriole                                # OHarmPi developer phone
    ├── audio_alsa                        # media content directory
        ├── audio.param.c                          
    ├── cfg                               # Configuration file
    ├── updater                           # Update file
    ├── build.gn                          # Compile script file
    ├── bundle.json                       # Compile script file
    ├── config.gni                        # Configuration content file
    ├── device.gni                        # Device script file
    ├── ohos.build                        # Compile script file                   
```

#### Compile-build method

* xxxx
* xxxx
* xxxx


#### Instructions for use

1. xxxx
2. xxxx
3. xxxx


#### Contribute

1. Fork the local warehouse
2. Create a branch of Feat_xxx
3. Submit the code
4. Create a Pull Request


#### related position

1. [vendor_hardmony](https://gitee.com/openharmony-sig/vendor_hardmony)
2. [device_soc_spreadtrum](https://gitee.com/openharmony-sig/device_soc_spreadtrum)


